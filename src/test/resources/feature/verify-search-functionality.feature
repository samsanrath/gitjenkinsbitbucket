Feature: HD-0001-Search functionality
#This test will validate search functionality of home page


@Regression
@HD-0001-1
Scenario: Verify that user can search content by giving valid keyword with keystroke
Given that I'm a user who is in healthdirect home page
When I enter keyword as "general"and hit enter
Then I should be able to see search results
Given I'm in healthdirect home page
When I close the healthdirect browser window
Then healthdirect Browser window should be closed

@Regression
@HD-0001-2
Scenario: Verify that user can search content by giving invalid keyword with keystroke
Given that I'm a user who is in healthdirect home page
When I enter keyword as "sam san"and hit enter
Then I should be able to see search suggesions
Given I'm in healthdirect home page
When I close the healthdirect browser window
Then healthdirect Browser window should be closed